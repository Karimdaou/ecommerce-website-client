import Home  from "./containers/home";
import './styles/reset.css'
import './styles/app.css'
import Login from './components/login.js'
import Register from './components/register.js'
import SecretPage from './components/secretpage.js'
import Admin from './containers/admin.js'
import EmailPage  from "./containers/emailpage";
import AddressPage  from "./containers/addresspage";
import CheckoutPage  from "./containers/checkoutpage";
import Thankyou  from "./containers/thankyou";
import SingleProductPage  from "./containers/singleproductpage";
import CategoryPage  from "./containers/categorypage";
import Cart  from "./containers/cart";
import Header  from "./components/header";
import Footer from "./components/footer.js";
import CountrySelect from './components/countryselect'
import React, { useState, useEffect }  from 'react';
import Axios from 'axios';
import {
  BrowserRouter as BrowserRouter,
  Route,
  Link,
  Redirect
} from 'react-router-dom'
import {URL} from "./config.js";
import {Helmet} from "react-helmet"



function App(props) {

  var [total, setTotal] = useState(0);
const [checkout,setCheckout] = useState(false)
const [address,setAddress] = useState(false)
const [stock,setStock] = useState(false)
const [address1,setAddress1] = useState({
})
const [products, setProducts] = useState ([]);
const [region, setRegion]= useState('')
const [country, setCountry]= useState('')

var changeCheckout = (arg) => {
  setCheckout(arg)
}

var changeAddress = (arg) => {
  setAddress(arg)
}

var changeStock = (arg) => {
  setStock(arg)
}

  var handleLoadCart = async () => {
    if (localStorage.getItem('session') != null) {
      var a = JSON.parse(localStorage.getItem('session'));
          try{
            const response =  await Axios.post(`${URL}/products/getproducts/category`,{
                      array : a
                })
                a.forEach(ele=>{
                  var index = response.data.message.findIndex(ele2 => ele2.product === ele.product)
                  return ( ele.price = response.data.message[index].price,
                           ele.stock = response.data.message[index].stock,
                           ele.image = response.data.message[index].image)
                })
              setProducts(a)
          }
          catch( error ){
            console.log(error)
          }   
}}

var changeQtyCart = (index, action) => {
  var temp = products; 
  if (action === "+") {
    temp[index].qty = temp[index].qty + 1;
  } else if (action === "-"){
    temp[index].qty = temp[index].qty - 1;
  }
  setProducts([...temp]);
  localStorage.setItem('session', JSON.stringify(products));
};

var handleInputQtyCart = (e,idx) => {
  const temp = products
  products[idx].qty = Number(e.target.value)
  setProducts([...temp]);
  
};

const calculateTotalCart = () => {
  let tempTotal = 0;
  for (var ele of products) {
    tempTotal += ele.price * ele.qty;
  }
  setTotal(tempTotal);
};

var removeItemCart = (i) => {
  var array=products
  array.splice(i,1)
  setProducts([...array])
  updateStorageCart(i)
}

var updateStorageCart = (i) => {
  var session = JSON.parse(localStorage.getItem("session"));
  console.log(session[i])
  session.splice(i,1);
  localStorage.setItem('session', JSON.stringify(session));
}

const AuthenticatedRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    localStorage.getItem('token') ? (
      <Component {...props}/>
    ) : (
      <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }}/>
    )
  )}/>
)

var handleInputChangeAddress = (e,idx) => {
  const temp = address1
  temp[e.target.name] = e.target.value
  setAddress1(temp);
  console.log(address1,'address1')
  
};

var handleInputChangeAddressCountry = (e,idx) => {
  const temp = address1
  temp['Country'] = e
  setAddress1(temp);
  console.log(address1,'address1')
  setCountry(e)
  
};

var handleInputChangeAddressState = (e,idx) => {
  const temp = address1
  temp['State'] = e
  setAddress1(temp);
  setRegion(e)
  console.log(address1,'address1')
  
};

    return (
      <div>
      <Helmet>
        <title>Tweeny</title>
        <meta charSet="utf-8"/>
        <meta name="description" content="Example implementation of Stripe checkout with React.js"/>
        <meta name='keywords' content='ecommerce and shit'/>
        <meta name='viewport' content='width=device-width, initial-scale=1'/>
      </Helmet>

      <BrowserRouter>
        <Header products={products} />
        
          <Route exact path="/home"  render={(props) => <Home {...props} isAuthed={true} />}/>
          <Route exact path="/thankyou"  render={(props) => <Thankyou {...props} isAuthed={true} />}/>
          <AuthenticatedRoute path="/admin"  component={Admin} />
          <Route path='/emailpage' render={(props) => <EmailPage {...props} isAuthed={true} />}/>
          <Route path='/checkoutpage' render={(props) => checkout 
                                                                  ?  <CheckoutPage {...props}
                                                                        isAuthed={true} 
                                                                        total={total}
                                                                        products={products}
                                                                        address1={address1}
                                                                         />
                                                                  : <Redirect to={'/cart'}/>} />
          <Route path='/addresspage' render={(props) => address&stock 
                                                                  ?  <AddressPage {...props}
                                                                        changeCheckout={changeCheckout}
                                                                        isAuthed={true} 
                                                                        handleInputChange={handleInputChangeAddress}
                                                                        address1={address1}
                                                                        handleInputChangeState={handleInputChangeAddressState}
                                                                        handleInputChangeCountry={handleInputChangeAddressCountry}
                                                                        country={country}
                                                                        region={region}
                                                                         />
                                                                  : <Redirect to={'/cart'}/>} />                                                        

          <Route path='/login'       render={(props) => <Login {...props} isAuthed={true} />}/>
          <Route path='/register'                   render={(props) => <Register {...props} isAuthed={true} />}/>
          <Route path='/singleproductpage/:product' render={(props)=> <SingleProductPage
                                                       {...props}
                                                       handleLoadCart={handleLoadCart}
                                                       products={products}/>}/>
          <Route exact path='/categorypage/:category' render={(props)=> <CategoryPage
                                                        {...props}
                                                        />}/>
          <Route path='/cart' render={(props) => <Cart 
                                                      {...props}
                                                      changeStock={changeStock}
                                                      changeAddress={changeAddress}
                                                      handleLoad={handleLoadCart} 
                                                      products={products} 
                                                      total={total}
                                                      checkout={checkout}
                                                      isAuthed={true}
                                                      changeQty={changeQtyCart}
                                                      handleInputQty={handleInputQtyCart} 
                                                      calculateTotal={calculateTotalCart}
                                                      removeItem={removeItemCart}/>}/>
      <Footer/>
      </BrowserRouter>
      
      </div>
    )
} 
export default App