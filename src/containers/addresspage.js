import React, {useState, useEffect, useCallback } from 'react';
import Axios from 'axios';
import CountrySelect from '../components/countryselect'
import '../styles/address.css';




var AddressPage = (props) => {

  

useEffect(()=>{
    props.changeCheckout(false)
    },[])




    return (  <div> 
        <div className='spacerx'></div>

        <div className='titlexx'>Delivery Address</div>


        <div className='mainGrid'>
        <div width='10em'></div>


        <div className="mainBoxz">
        <div className='topBox'></div>


        <div className='gifBox'>
            <div>    
                    <div className='inputx'>
                        <div className='inputext'>Address Line</div>
                        <input type = {"text"} name={'Address Line 1'}  onChange ={(e)=>props.handleInputChange(e)}/>
                    </div>
                    <div className='inputx'>
                        <div className='inputext'>City/Town</div>
                        <input type = {"text"} name={'City/Town'}  onChange ={(e)=>props.handleInputChange(e)}/>
                    </div>
            </div>
            <div>
            <div className='planex'><iframe src="https://giphy.com/embed/jqZeLeJ6fiIAT2CNJ9" width="100%" height="100%"  frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
            </div></div>
        </div>

        <div className='lowbox'>
        <div className='lowleft'>
            <CountrySelect handleInputChangeState={props.handleInputChangeState} handleInputChangeCountry={props.handleInputChangeCountry} country={props.country} region={props.region}></CountrySelect>
            <div className='emailx'><div className='inputext'>Phone Number</div><input onChange ={(e)=>props.handleInputChange(e)} name={'Phone Number'}></input></div>
        </div>
            <div className='lowright'>
            <div className='zipx'><div>Zip code</div><input name={'ZIP code'} onChange ={(e)=>props.handleInputChange(e)}></input></div>
            {/* <div className='shippedx'><iframe src="https://giphy.com/embed/xASvRtBWiAO8ABBM2K" width="90%" height="90%"  frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div> */}

        </div>
        </div>

        </div></div>
{/* 

    <form>
        <input type = {"text"} name={'addressline1'}  onChange ={(e)=>props.handleInputChange(e)}/>
        <input type = {"text"} name={'addressline2'} onChange ={(e)=>props.handleInputChange(e)}/>
        <input type = {"text"} name={'city'} onChange ={(e)=>props.handleInputChange(e)}/>
        <input type = {"text"} name={'country'} onChange ={(e)=>props.handleInputChange(e)}/>
        </form> */}


<div className='buttonx1'>
<button onClick ={()=>{props.history.push('/checkoutPage');props.changeCheckout(true)}}>Go to Checkout</button>
</div>
</div>


    )


    
}



    export default  AddressPage    