import React,{useEffect} from "react";
import AddProduct from "../components/addproduct.js";
import DeleteProduct from "../components/deleteproduct.js";
import ViewProduct from "../components/viewproduct.js";
import ViewProducts from "../components/viewproducts.js";
import UpdateProduct from "../components/updateproduct.js";
import Axios from 'axios';
import {URL} from "../config.js";
import '../styles/admin.css';


var Admin = (props) => {

    
    const token = JSON.parse(localStorage.getItem('token'))
	useEffect( () => {
		if( token === null )return props.history.push('/')
	},[token,props.history])
	const verify_token = async () => {
		try{
           const response = await Axios.post(`${URL}/admin/verify_token`,{token})
           return !response.data.ok
           ? props.history.push('/')
           : null
		}
		catch(error){
			console.log(error)
		}
	}
	verify_token()
    return (
        <div>
            <div>
            <div className='adminhead'>Admin Area</div></div>
            <AddProduct/>
            <DeleteProduct/>
            <ViewProduct/>
            <ViewProducts/>
            <UpdateProduct/>
            <div className='addboxinnerb'><button onClick={()=>{localStorage.removeItem('token');props.history.push('/')}}>Logout from admin area</button></div>
        </div>
    )
}

export default Admin