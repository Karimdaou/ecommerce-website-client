import React, { useState, useEffect }  from "react";
import ProductList from "../components/productlist.js";
import BestSeller from "../components/bestseller.js";
import '../styles/reset.css';
import '../styles/home.css';
import Axios from 'axios' ;
import Header from "../components/header.js";
import { NavLink } from "react-router-dom";
import {URL} from "../config.js";
import Footer from "../components/footer.js";
import MyCarousel from '../components/mycarousel.js'




var Home = (props) => {
console.log(props,"homeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee")

const [ products , setProds ] = useState([])

useEffect(()=>{
    
  var handleLoad = async () => {

  try{
    const response =  await Axios.get(`${URL}/products/displayprods`)
        var xprod = products
        xprod= response.data.message        
        setProds(xprod)
  }
  catch( error ){
    console.log(error)
  }
}

handleLoad()
},[])

    return (
      <div class="wrapper">
      <div class="borderClass">
    <div>

      <div className='homejoke'><h1 className='hiText2'>Think of us like Armani, but with a sense of humor</h1></div>
      
      <div className='pugimg'><img src='https://images-na.ssl-images-amazon.com/images/I/71xsARdiz2L._UX679_.jpg'></img></div>  
    <div>
      <span>
        <div className='productheader'>Our Products</div>
    <ProductList list={products} {...props}/>
    </span>
    </div>


    <div className='carouselxz'>
    <MyCarousel />
    </div> 


    <div className='productheader'>
      <h1 >Best Sellers</h1>
    </div>
    <div >
      <span>
    <BestSeller list={products}/>
    </span>
    </div>
    </div>
   
    </div>
    </div>
       
      );}

export default Home;