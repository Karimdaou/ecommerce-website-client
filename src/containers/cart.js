import React, { useState, useEffect }  from 'react';
import Axios from 'axios'
import '../styles/cart.css';


var Cart = (props) => {

    var stockCheck = []

useEffect(()=>{
    props.handleLoad()
    props.changeAddress(false)
    props.changeStock(false)
    window.scrollTo(0,0)
    },[])


useEffect(() => {
    props.calculateTotal();
  });

  useEffect(() => {
    checkStock();
  },[]);

  useEffect(() => {
    checkStockx();
  },[props.products]);

const checkStockx = () => {
    checkStock()
}

const checkStock = () => {
    // var stockCheck = []
    props.products.forEach(ele => {
        if (ele.qty > ele.stock) {
            stockCheck.push({stock:false})
            ele.stockCheck = 'Appologies, we do not have enough stock of this item'
        } if (ele.qty <= ele.stock) {
            ele.stockCheck = 'In Stock'
            stockCheck.push({stock:true})
        }
        console.log(stockCheck)
        var x = stockCheck.findIndex(ele=> ele.stock === false)
        if (x === -1) {
            props.changeStock(true) 
        } else if (x != -1) {
            props.changeStock(false)
        }});}
        
        return (
        <div className='cartmain'>
            <div>
            {
            props.products.map((ele, i) => {
                return (<div key={i} className='productz'>
                    <div className='productx'>
                    <div className='producty'>
                        <div className='cartimg'><div><img height='100' width='100' src={ele.image}/></div></div>
                        <div>
                            <div className='productname'>{ele.product}</div>
                            <h4>{ele.price}$</h4>
                            <h2>Quantity: {ele.qty}</h2>
                            <div className='stockx'>{ele.stockCheck}</div>
                            <div className='deleteqty'>
                                <div>
                <button onClick ={()=>props.changeQty(i,"+")}>+</button>
                <input min= {"0"} type = {"number"} value={ele.qty} onChange ={(e,idx)=>props.handleInputQty(e,i)}/>
                <button disabled = {ele.qty === 1} onClick ={()=>props.changeQty(i,"-")}>-</button></div>
                <div><button onClick ={()=>props.removeItem(i)}>Delete Item</button></div></div>
                            </div>
                        <div className='pricex'><h2>Total</h2>
                             <h2 >{ele.qty*ele.price}$</h2></div>
                </div></div></div>
                )})
            }
            </div>
            <div className='go2check'>
            <div>
            <div>
                <div> Total</div>
                <div>{props.total}$</div>
            </div>
            <div className='shipping'> {props.total > 500 ? <h1> free shipping </h1> : <h1>no free shipping</h1> }</div>
            </div>
                <div className='glow-on-hoverx'>
            
            <div className='checkoutx'>
              <button onClick ={()=>{props.history.push('/addressPage');props.changeAddress(true)}}>Go to Checkout</button>
            </div>
            </div>
            </div></div>
            
        )
    
    

    
}
export default Cart