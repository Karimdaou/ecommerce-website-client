import React, { useState, useEffect } from "react";
import ImageGallery from 'react-image-gallery';
import Axios from 'axios' ;
import '../styles/singleproductpage.css';
import '../styles/singleproductpage.scss';
import {URL} from "../config.js";

import MyCarousel from '../components/mycarousel.js'



 
var SingleProductPage = (props) => {



  const [ obj , setObj ] = useState({})

  const [ qty , setQty ] = useState(1)




console.log()


  const handleChange = e => {
    setQty(Number(e.target.value))
}

useEffect(() => {
  props.handleLoadCart();
  window.scrollTo(0,0)
},[]);


  var addToStorage = () =>
{
  if(qty ===0)
  return alert("you can't add zero items")
  else {
  if (localStorage.getItem('session') === null) {
    var a=[]
    a.push({product:obj.product,qty:qty});
    localStorage.setItem('session', JSON.stringify(a));
  } else {
    var a = JSON.parse(localStorage.getItem('session'));
    var x = a.findIndex(ele=> ele.product === obj.product)
    if (x ===-1) {
      a.push({product:obj.product,qty:qty});
    } else {
      a[x].qty += qty
    }
    localStorage.setItem('session', JSON.stringify(a));
  }
}
}



 var handleLoad = async () => {
  
  try{
    const response =  await Axios.post(`${URL}/products/getproductpage`,{
              product: props.match.params.product,
        })
        var tobj = obj
        tobj.product= response.data.message.product
        tobj.images= response.data.message.images
        tobj.description= response.data.message.description
        tobj.price= response.data.message.price
        setObj({...obj,...tobj})

  }
  catch( error ){
    console.log(error)
  }
}

useEffect(()=>{
handleLoad()
},[])

    return (<div>
      <div className="mainHeaderx">
        <div className='titleBox'>
          <p className="titleClass">{obj.product}</p>
        </div>
    </div>
    <div className="mainBox">
    <div className='leftcarou'>
      <ImageGallery items={obj.images} />
    </div>
    <div className='singletext'>
      <p>{obj.description}</p>
    </div>
    <div className="buttonBox">
      <form>


     <div className="glow-on-hoverxy">
       <div className='buttonTextx'>
         <div className="priceX"><h1>{obj.price}$/item  </h1></div>
         <div className="shippingX"><p>10.40$ shipping</p></div>
         <div className="shippingX"><p>delivery in 3-5 days</p></div>
         </div>
       </div>
      <div className='singlebox'>

        
      <div className='singleinput'><input onChange={handleChange} type="number" min="1" defaultValue="1"></input></div>
      <div className='singleinput'>
         <button class="button1" onClick={addToStorage} >Buy Now!</button>
       </div>
     <div className='singleinput'>
       <button class="button1" onClick={addToStorage} >Add to Cart</button>
       </div>
       
       </div>
     </form>
    </div>
    </div>
    <div className='carouselxz'>
    <MyCarousel />
    </div>  
      </div>
    );
  }

export default SingleProductPage;