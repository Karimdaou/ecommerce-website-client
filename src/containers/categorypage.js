import React, { useState, useEffect } from "react";
import ImageGallery from 'react-image-gallery';
import Axios from 'axios' ;
import ProductList from "../components/productlist.js";
import BestSeller from "../components/bestseller.js";
import '../styles/reset.css';
import '../styles/home.css';
import Header from "../components/header.js";
import {URL} from "../config.js";


 
var CategoryPage = (props) => {

  const [ products , setProds ] = useState([])

  var [ selector, setSelector] = useState('')

  var handleSelect = (e) => {
      setSelector(e.target.value)
  }

  var handleLoad = async () => {
    try{
      const response =  await Axios.get(`${URL}/products/getproducts/${props.match.params.category}`)
          var xprod = products
          xprod= response.data.message
          setProds(xprod)
    }
    catch( error ){
      console.log(error)
    }
  }
 
useEffect(()=>{ 
    handleLoad();
    window.scrollTo(0,0)
},[])

useEffect (()=>{
  handleLoad()
},[props.match.params.category])


  return (
    <div className="wrapper">
    <div className="borderClass">
  <div>
    <div className='productheader'>Our {props.match.params.category}</div>  
  <div className='selectorx'>
      <select onChange={handleSelect}>
          <option value = 'priceAsc'>Cheapest First</option>
          <option value = 'priceDes'>Most Expensive First</option>
          <option value = 'alphAsc'>A to Z</option>
          <option value = 'alphDes'>Z to A</option>
      </select>
      </div>
      <div>
    <span>
  <ProductList list={products} {...props} selector={selector}/>
  </span>
  </div>
  <div  className='productheader'>Best Sellers</div>
  <div >
    <span>
  <BestSeller list={products}/>
  </span>
  </div>
  </div>

  </div>
  </div>
     
    );
  }
 
export default CategoryPage;