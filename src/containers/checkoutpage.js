import React, { Component } from "react";
import { Elements, StripeProvider } from "react-stripe-elements";
import CheckoutForm from "../components/checkoutform.js";
import {pk_test} from "../config.js";
import '../styles/checkoutpage.css';
import '../styles/checkoutpage.sass';


class CheckoutPage extends Component {

  componentDidMount(){
    console.log(this.props.products,'component');
    window.scrollTo(0,0)
  }

  render() {
    return (
      <div className='mainxy'>

        <div>
        <div className='producttitle'>
        <div ><iframe src="https://giphy.com/embed/XbJOF7vXUBBTQalus6" width="100%" height="100%"  frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div>
          <div className='urorder'>Your order</div>
          <div ><iframe src="https://giphy.com/embed/3s2aD5KoMqbSfeKdvY" width="100%" height="100%"  frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div>
          </div>
      {
            this.props.products.map((ele, i) => {
                return (
                  <div key={i} className='productz1'>
                    <div className='productx'>
                    <div className='producty'>
                        <div className='cartimg'><img  height='100' width='100' src={ele.image}/></div>
                        <div>
                            <div className='productname'>{ele.product}</div>
                            <h4>{ele.price}$</h4>
                            <h2>Quantity: {ele.qty}</h2>
                            
                            </div>
                        <div className='pricex'><h2>Total</h2>
                             <h2 >{ele.qty*ele.price}$</h2></div>
                </div></div></div>
                )})}
     </div>


      
      <div className='addressz1'>
        <div className='producttitle'>
            <div ><iframe src="https://giphy.com/embed/RkHFJCWvv0WnUjPX98" width="100%" height="100%"  frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div>    
            <div className='urorder'>Your address</div>
            <div ><iframe src="https://giphy.com/embed/elVKsgG1zfTSjPfRlb" width="100%" height="100%"  frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div>
        </div>
      {Object.entries(this.props.address1).map(([key, value]) => {
        return(
          <div className='addressy'>
            <div className='addressx'>
              <div className='addressitem'><div>{key}:</div><div>{value}</div></div>
          </div></div>
        )
})}</div>
      


      <div className='stripx'>
      <div className='producttitle'>
            <div ><iframe src="https://giphy.com/embed/8mzhcmWskIFDYeA2qm" width="100%" height="100%"  frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div>    
            <div className='urorder'>Payment</div>
            <div ></div>
        </div>
      <StripeProvider apiKey={pk_test}>
        <div className="example">
          <Elements>
            <CheckoutForm   
                            total={this.props.total}
                            products={this.props.products}
                            address1={this.props.address1} />
          </Elements>
        </div>
      </StripeProvider>
      </div>


      </div>
    );
  }
}


export default CheckoutPage;
