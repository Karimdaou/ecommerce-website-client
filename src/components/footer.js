import React,{ useState, useEffect }  from 'react'
import '../styles/reset.css';
import { NavLink,Link } from "react-router-dom";
import '../styles/footer.css';
import '../styles/footer.scss';

var Footer = (props) => {

    var shoes = 'shoes'
    var shorts = 'shorts'


    return ( 
    <div className='mainfooterk'><div className="mainFooter">
      <div className='mainFooterx'>
          <div className='leftBox'>
              <div className='foottitle'>Areas Serviced</div>
              
              <div className='itemx'>North America</div>
              <div className='itemx'>Europe</div>
              <div className='itemx'>Middle East</div>
              <div className='itemx'>Far East</div>
              <div className='itemx'>South America</div>
              </div>
          
          <div className='leftBox'>
              <div className='foottitle'>Delivery Times</div >
              
              <div className='itemx'>1-3 days</div>
              <div className='itemx'>1-3 days</div>
              <div className='itemx'>3-5 days</div>
              <div className='itemx'>Depends on order</div>
              <div className='itemx'>Depends on order</div>
              </div>
          
          <div className='leftBox'>
              <div className='foottitle'>Go To:</div >
              
        <div className='itemx'><Link style={{ textDecoration: 'underlined', color: 'white'  }} to={'/home'}>Home</Link></div>
        
        <div className='itemx'><Link style={{ textDecoration: 'underlined', color: 'white' }} to={`/CategoryPage/${shoes}`}>Shoes</Link></div>
        
        <div className='itemx'><Link style={{ textDecoration: 'underlined', color: 'white'  }} to={`/CategoryPage/${shorts}`}>Shorts</Link></div>
      </div>
      
      </div>
      <div className='borderx'>
      <div className='mainFootery'>
    
      <div className='leftBox'>
              <div className='foottitle'>Contact US</div >
              
              <div className='itemx'>PHONE: 054 / 9923</div>
              <div className='itemx'>E-MAIL: orders@Tweeny.com</div>
              <div className='itemx'>ON SOCIAL MEDIA:</div>

              <div className='itemx'><div class="flex-center">
                    <i class="fa fa-twitter fa-4x icon-3d"></i>
                    <i class="fa fa-facebook fa-4x icon-3d"></i>
                    <i class="fa fa-instagram fa-4x icon-3d"></i>
                    <i class="fa fa-whatsapp fa-4x icon-3d"></i>
</div></div>
              
              </div> 
              </div>
      </div></div>

      <div className='footween'>Tweeny</div>

      </div>


    )
}


export default Footer