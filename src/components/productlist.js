import React from "react";
import '../styles/reset.css';
import '../styles/home.css';


var ProductList = (props) => {


    var xlist = props.list

    if(props.selector === 'alphAsc')
    {
        xlist.sort(function(a, b){
            if(a.product < b.product) { return -1; }
            if(a.product > b.product) { return 1; }
            console.log(xlist)
            return 0;
            
        }) 
    } 
    else if(props.selector === 'alphDes')
    {
        xlist.sort(function(a, b){
            if(a.product > b.product) { return -1; }
            if(a.product < b.product) { return 1; }
            console.log(xlist)
            return 0;
        }) 
    }
    else if(props.selector === 'priceAsc') 
    { 
        xlist.sort((a, b) => parseFloat(a.price) - parseFloat(b.price));
    } 
    else if  (props.selector === 'priceDes') 
    {
        xlist.sort((a, b) => parseFloat(b.price) - parseFloat(a.price));
    }

    // {()=>props.history.push(`/singleproductpage/${product.product}`)}

    return (
        <div  className="mainFlex">
          {
          xlist.map((product, i) => {
              return  <div class = "productBox"  onClick ={()=>props.history.push(`/singleproductpage/${product.product}`)} >
                  <div class = "name"><h1>{product.product}</h1></div>  
              <img src = {product.image} class = "img"></img>
              <div>
              
              <h2 class = "price">${product.price}</h2>
              {product.onSale === true ?<img src = "https://cdn0.iconfinder.com/data/icons/shopping-icons-4/110/Tag-Sale-512.png" class = "onSaleTag"></img> : null }
              </div>
              </div> 
          
              
          })}
        </div>
    );}
    
export default ProductList;