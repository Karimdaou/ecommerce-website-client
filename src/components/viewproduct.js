import React , { useState } from 'react'
import Axios from 'axios' 
import {URL} from "../config.js";

const ViewProduct = () => {
	const [ form , setValues ] = useState({
        product: ''
	})
    const [ message , setMessage ] = useState({})
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
			const response =  await Axios.post(`${URL}/products/getproduct`,{
                product: form.product,
                _id: form._id
			})
			
	        setMessage(response.data.message)
		}
		catch( error ){
			console.log(error)
		}

    }

	return( <div><form onSubmit={handleSubmit}
	             onChange={handleChange}
	             className='form_container'>
					 <div className='addboxy'>
						 <div className='addyboxinner'>
						 <div className='addtitle'> View a Product from DB</div>
							 <div className='addinputx'>
             <label>Product</label>
             <input name="product"/>
			 <div>or</div><div></div>
			 <label>ID</label>
             <input name="_id"/></div>
		     <button>Find</button>

		     <div className='adminmessage'>

				{ message ? Object.entries(message).map(([key,value])=>{
				return <div className='viewmsg'>
					<div>{key}</div>
					<div>{value.toString()}</div>
				</div>
				}
				)
				: <div className='viewnotfoundx'>
					<div> Item does not exist</div>
						
					</div>
				}

				<div></div>
				 {/* <h4>{message}</h4> */}
				 </div>
			 </div></div>
	       </form></div>

    );}

export default ViewProduct
