import React , { useState } from 'react'
import Axios from 'axios' 
import {URL} from "../config.js";
import '../styles/admin.css';

const AddProduct = () => {
	const [ form , setValues ] = useState({
		catID    : '',
		price : 0,
        color: '',
        product: ''
	})
	const [ message , setMessage ] = useState('')
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
			const response =  await Axios.post(`${URL}/products/add`,{
	            catID    : form.catID,	
			    price : form.price,
                color: form.color,
                product: form.product
	        })
	        setMessage(response.data.message)
	        //console.log(response)
		}
		catch( error ){
			console.log(error)
		}

	}
	return <form onSubmit={handleSubmit}
	             onChange={handleChange}
	             className='form_container'>
					 <div className='addbox'>
						 
						 <div className='addboxinner'>
						 <div className='addtitle'> Create a Product in DB</div>
						 <div className='addinput'>
             					<label>Product Name</label>
             					<input name="product"/>
			 				</div>
							 <div className='addinput'>
	         					<label>Category</label>
		     					<input name="catID"/>
			 				</div>
							<div className='addinput'>
		     					<label>Color</label>
		     					<input name="color"/>
			 				</div>
			 				<div className='addinput'>
		     					<label>Price</label>
		     					<input type="number" name="price"/>
			 				</div>
			 				
		     <div className='addbutton'><button>create</button></div>
		     
			 <div className='adminmessage'><div>{message}</div></div>
			 </div></div>
	       </form>
}

export default AddProduct
