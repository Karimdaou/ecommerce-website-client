import React, { useState, useEffect, Component }  from "react";
import {
  CardNumberElement,
  CardExpiryElement,
  CardCVCElement,
  injectStripe,
  StripeProvider,
  Elements
} from "react-stripe-elements";
import axios from "axios";
import {URL} from "../config.js";
import '../styles/checkoutpage.css';
import '../styles/checkoutpage.sass';



class CheckoutForm extends Component {

  routeChange = () => {
    window.location.href="http://localhost:3000/thankyou"
  }

  address1=this.props.address1
  products=this.props.products
  
  static getDerivedStateFromProps(props,state){
    console.log(state,'from derived')
    return {amount:props.total}
}

  state = {
    errorMessage: "",
    cardNumber: false,
    cardExpiry: false,
    cardCvc: false,
    amount: 0,
  };



  handleChange = ({ elementType, complete, error }) => {
    if (error) return this.setState({ errorMessage: error.code });
    return this.setState({ [elementType]: complete });
  };

  handleInputChange = e => this.setState({ [e.target.name]: e.target.value });

  handleSubmit = async e => {
    e.preventDefault();
    const { cardNumber, cardCvc, cardExpiry } = this.state;
    if (!cardNumber || !cardCvc || !cardExpiry) return alert("Please fill all the fields");
    const fullname = this.state.name;
    const { name, email, phone, pc, amount } = this.state;
    const {address1,products} = this
    if (this.props.stripe) {
      const { token } = await this.props.stripe.createToken({ name:fullname, email });
      //console.log('token ====>',token)
      const response = await axios.post(`${URL}/payment/charge`, {
        token_id: token.id,
        amount,
        name,
        email,
        phone,
        pc,
        address1,
        products
      });
      console.log('response ====>',response.data)
      //response.data.status === "succeeded" ? alert("Payment successful") : alert("Payment error");
      // alert("Payment successful")
      this.routeChange()
      
    } else {
      alert("Stripe.js hasn't loaded yet.");
    }
  };

  render() {
    return (
<form className="checkout" onSubmit={this.handleSubmit}>
        
  <div class="containerxy">


    <div class="card__container">
    
      <div class="card">

        <div class="row paypal">
            <div class="left">
                <input id="pp" type="radio" name="payment" />
                <div class="radio"></div>
                <label for="pp">Paypal</label>
            </div>
            <div class="right">
                <img height='100' width='100' src="https://www.paypalobjects.com/webstatic/mktg/logo/pp_cc_mark_111x69.jpg" alt="paypal" />
            </div>
        </div>

        <div class="row credit">
        <div class="left">
                    <input id="cd" type="radio" name="payment" />
                    <div class="radio"></div>
                    <label for="cd">Debit/ Credit Card</label>
                </div>
                <div class="right">
                    <img height='100' width='100'  src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Old_Visa_Logo.svg/524px-Old_Visa_Logo.svg.png" alt="visa" />
                    <img height='100' width='100'  src="https://brand.mastercard.com/content/dam/mccom/brandcenter/thumbnails/mastercard_vrt_pos_92px_2x.png" alt="mastercard" />
                    <img height='100' width='100'  src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8QDw8PDw0QDg8NDw0PDRANEA8ODw8PFhEWFhURExcZHzQgGBolGxMVITEhJTUrLi4uFx8zODMuNygtLi4BCgoKDg0OGhAQGi0mHx0tLi0tNi0uNzIvLy8vLS0tLS0tLjAvLS0tLTU1LSstLS8tLS0rKy0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAbAAEAAQUBAAAAAAAAAAAAAAAAAwECBAUGB//EAEcQAAIBAwICBAkHCQYHAAAAAAABAgMEEQUSBiETMUHRByJRU2FxkZOxFBc1UlRzgRYjJUNydJKhshUkMmLC8DRCgoOis8H/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAQIEBQMG/8QANxEBAAIBAgEKBQIFBQEBAAAAAAECAwQREhMUFSExM1FhgaEFQXGR8DJSIiNCotEkscHh8TRT/9oADAMBAAIRAxEAPwD3EAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADl9d1avSryhTmlFRg0tsXza59Zv0+Cl6b2hzNVqMlMnDWephLXrrzi/gh3HtzXF4PDnmbx9lf7euvOL+CHcRzbF4HPM3if29decX8EO4c1xeBzzN4+y16/dedX8EO4nmuLwOeZvH2Ry4hu/Or+CHcTzXF4I55m8fZFLiS886v4IdxPNMXgidbm8fZDPie9X65e7h3Fo0mHw91efZ/H2QVOLL5frl7un3Fo0eHw91J1+fx9mPPjG/X6+PuqfcW5lh8PdTpDUePsilxpqHn4+6p9w5jh8PdPSGo8fZHLjfUfPx91S7ieY4fD3T0hn8fZFLjnUvtEfdUu4cxw+Hunn+fx9lj471P7RH3NLuHMcHh7nP8/j7LHx7qf2iPuaXcTzHB4e6ef5/H2dBwHxVe3V6qNetGdPoqs8KnCHjLbjml6TLrNLix4+Ksde7VotVly5OG09Wz0g5TrAAAAAAAAAAAAAAOL4mX96l+zT+B1NL3cerja3vp9GujE0Mq9QK7pHAbiOUSyEUkTCEE0WQx6iLQ85YtRF4Ulh1UXh5yx5EoQzQXhDMlKJgRsmB1fgu+kV9xW+MTF8R7n1hv+Hd96S9iOE7wAAAAAAAAAAAAADjeJf+Kl+zT+B1NL3TjazvZ9GBA9pZl6QSNBEo5omEIJFkSgqFkMeoTCksWoekKSw6xeHlLGmShDMLQgkSsikBGyYHV+C76RX3Fb4xMXxHufWG/wCHd96S9iOE7wAAAAAAAAAAAAADjeJf+Kl+zT+B1NL3bjazvZ9Gvgz2lmSJhO4whFNkwhDItCEFRlkMeoTCksSoekKSxKxeHlLGmShDMlZBILIpEiNiB1fgu+kV9xW+MTF8R7n1hv8Ah3fekvYjhO8AAAAAAAAAAAAAA4viZ/3qX7NP4HU0vdONre9n0a6MjQyr1MjZKjmNkLJMkRSZKqCbLIY9RloUli1GXhSzDqsvDyY8iRDNhaEMyVkUgI2TA6vwXfSK+4rfGJi+I9z6w3/Du+9JexHCd4AAAAAAAAAAAAAByOsWkq1+6UeTcINt9UUo9b/32nSw5Ipg4pcnUY5yajhjwUhw/l4V1ScufJc38ROq2jeayiNHv1ReFXoEko77inTclnE+T9XXzHOonsrMp5lMfqtEMa80mdN0kpxqKs9sZQ6s/wC/gy9M9bRO8bbPLJprUmu078TInw/h4d1RT7U+T9mSkarq34Zek6Lbqm8I6vDuHiV3Ri/JLk/iTGr37KyidFt1TeFkeGtz2xvKEm+yPNv8Mkzq9uuayiNFxTtF4Y8+Go/b7f2rvLRq5/ZKvMo//SFkeDak34l1QnHnmUcy2vsWETOurXtrKOjrW7LQgfBFSak6V3QquPZHPX5G03gnn9Yn+Kswp0ZaYma3iUM+CZLlPULWEu2Lbyva0Tz6J64pJ0bMdt4W1+BHF7Z6jbQl14nmLx6mxGvieuKStPw6a9U3j89Vk/B7WW91LujSjGSSnNNRkmk0855c21j0DpCs9lZlPRt47bRCOPg9qz29FeUK0XPbOVNOSp+K3l4fPmksf5iZ+IVjtrMEfDrT2WiVH4PJveo6hbzlTUnOMU3KOPKk+Q6Qjq3pPWno6eva8dTE8Fj/AEiv3et8Yl/iPc+sKfDu+9JexnCd4AAAAAAAAAAAAABpNUat/lNzy31I06VL0cu/n/0mnF/M4cfyjrljz/yovk+c7RDnnbOhStrqMk6m/dNZTaT5xX4pNP1m3j5S1sc9jncnyVKZY7d+tsOJNtarZ4fi1lhP0SlHvPHTb0rfyaNZtkvj8J/52S6BeKnCtRrJbrRzqRz2JZTx+L/8iuopxWi1f6ltLkilbUv/AEdf5+fNo69OUlC4n13FabXqTXP2vH4GusxG9I/phitWZ2yW/qlPxD0K1CTrwlOnshujT5Sb2csc/Keen4+Qjgnr/wC3pquDnM8cbxt8vozeH5WLuIdBQrQqJTcZVHmKW3n2+Q8tRy3JzxTGz20vN5yxwVmJ83M6T8gzW+Wqed/5vo9/Vl5zt/A3ZeX6uSc/Dzfe3Lb9vVs2fDFGEtQcrPfG2hBue983Fxxjyvxur1HhqbTGDbL+qWnR1rOp3w/pj8/3a3Tr6dGwv5U24ynXo090eTipZy15HhNfieuTHF81It8omXliyTj0+Sa9szEN5pHBtl8mp3FxvlJ01XqS6RxjHlu6l2JGXLrcvKTSnjs2Yfh+Hk4vft23/wCXOTsv7RhqN9OSU007aEpJNqPNxw/8iS9bZq4+QmmKPX8+rLyfOYyZZ9Pz6NlqOq/KtAbk81KM7ejVz1txqRxJ+uOGeOPFyer8p3l75MvK6Lr7Y2ifuweGLqemXFs6j/umpUKM93/LGcorn64yeH6JJ9hfUVjUUtt+qkypprTp713/AE3iGy4S+ktZ/wC//wC2R5anuMXp/s99L3+VoPBV9Ir93rf6TT8R7n1hl+G996PZDhO+AAAAAAAAAAAAAA0Wu6fWuKtKCjihB5nLck8vrwvUv5mrBlpjrM/Nh1OG+W9Y/pjta6lp9hVqVaFKVTpoKeG34u5cuXlwz2nLnrWL222lnrg097TSszvClvpF3m2U6a229TOd8X4jnGXl7MMm2fFtbae2EU02benFH6Z8fNka7o1SrW6ShjE1treMliSeMvy8sewpg1FaU4b/AC7HrqdLa9+Knz7U2saXKcLaFCKlGg5RfNLqwu3tymVw5orNpv8ANbUaebVpGPsqx9T0+8V67m3pwl4sYxc5Rx/gw+WS2PLinFwXlTNhzxn5THEM3TamourFV6VKNLxtzhjcuTxjxvLg8ssYOH+CZ3e2GdTN45SI2/PNqtF02raUbupXoU23KEqaltmmstPq6us0Z8tc1qxSWfT4LYaXteseTKWi1IanG4p0owobGpOLjHxnBp+L69p58vWdPwWnrenNrV1XKVj+H/r/AMazSuFartbyhcJUXWnTnRnujNKUctN47M8vxPXLq68pS1OvbteOHRW5K9L9W87wxq2l62rZ2W2jOjt6NSjOKm6f1dza5Y5c11F4y6XlOU691Zw6zk+S6tuxBc6BpNtO3t7uVZ3NaFPc4SexSk9uX9Vbs+wmuo1GSLXx7cMItptLjmtMm/FKK74Pvqfyy3t6aqW1xKk6blVgmtk1KLafbjdEmusxW4b3nrjyLaLNXjpSP4Z2+fhLoNS0BV9NtrGpthd0reEqCbT/ADlOMYyWe1eMk/XnsMtNRwZ7ZI/TM9fq130/Hgrin9UR1ejXcE8PXlrO7qXUFHpaDipdJGo3JNt5wz11eox5IrFPlLz0mnyY5tN/nDm/BV9Ix/d63+k1/Ee59YZPh3fej2Q4TvAAAAAAAAAAAAAAPO+MrupG9nGNWcVspcozlFdXkTOxo6VnFG8eLg6/JaueYiZ7I+bQ068k9yk4y8sW0/aa5rE9TFF5jriU3y+r56p7yfeV5OvhH2X5W/7p+7a6Vpt5cQdSjWajvcXurTi3LCy/5oz5cuLHbhtHs04cObLXipPvLKXDWorqqpeqvUX/AMKc6weHs9OZ6nx95Y97oeo0qcqjqSlGCzJU69SUsdrS7S1NRgtaK7dvkpk0uppWbb9nhMobHRNSr041YVZRjPnHpK9SEmuyWPIWvqMFLcMx2eEKY9NqclYtE9U+MyvnwnqbWHWTT607mq0/wwRzzT+HtC3MNV+7+6Uf5Las210rWFnc7qptfoXbkc7023Z7Qcx1e/b/AHSsq8HapJOMq0ZRfJxlc1ZJryNNcxGt08dcR7QToNVPVM/3Sihwhq2XBVHBRSafymag/RHHciZ1mm7dvZMaLVdm/wDdKOtwFqU3unOlOXJbp15yl7XEmNdgjqjf7E/D9RPXMx91avCGsrGKzll48W7q+Ly63ns5ERrNN4ey3M9VHz95UlwRq7ak68XKOdrd1Vco568PHIc90/Zt7QnmWp7d/eVn5G605OPTPCS8Z3dTY89i7f5Dnmm2329jmmq32395bfgfg67s7xV63RbOiqQ/NzcpZeMcseg8NXq8eXHw137XvpNJkxZOK23Y9EOY6gAAAAAAAAAAAAADy7jyeL+f3dH+k7mh7mPrL5z4l/8ARP0hoVUNezFur0o2N3VWNT9C3LT/AF6/qpmC8f6uv0/y6eOf9DafP/DBs1prpwda9uIVXFOpGEZOMZdqT2npfl4tPDSNvzzeFObcMTe8xPz/ADZsK1Gppzo3ltUncWlaMOkVR4e2XNZXZnPJ9j5HjFq6iJxXja0NE1tpZjNjmZpPbuut6VbVKk7iq5UrOgpqlTjJrc0s45dvVl/giLTTS1ileu09q1a31lpyW6qV7I/Pf7NPw5YWdejKd1fO3mp7Yx6WEMx2xe7EvS37DRqMmWltsdN4+jNpcWLJTfJfafqn0e6nCpqNtSuJ3FrG0upRlJtrMYrE15Otrl1lM1YmuO9o2tvD1wXmLZMdbb14ZYWh2Wn1aCndajUoVd004Ka/wp8nzTL5smat9qU3hTBjw2pvkyTE/VlcP1K3QavGFWpVs4WtyqU57sOaT2uOepuPNpeg888V48UzG1pmHrgm3BliJmaxE7NTpVPTZUk7u+uaNZuW6FOMpRSz4rT2vswe+Sc8W/grEw8MXITXfJeYny/8bXiKuqOmWkbK4rztKtev0taWYVG88oPkuWd3Lt2o8MFeLPblIjiiI2hpz24cFeStPDMzvLGtLDRpyjFatdRk2sdJFwjn0txwi18mprG/Jx+eqtKaaZ6skruLLmNTVatK/uq9va04x6DoVKSxsi4tRXXluWZYfVgjT1mMEWxViZnt3TqbROea5LTFY7Nm74FstNVz0lpqFa4qqnOPRVlszF4zJJxTeMdhn1d83BtekRHk06SuHj3peZnzegnNdIAAAAAAAAAAAAAB5T4QJfpCp93R+DO5oO5j6y+d+I//AET9Ic9uNjAbiUuy0ihUq6LcQpQlUnK4W2MVmTxKm3/I5uW0V1VZtO0bf5dTDS19FatY3nf/AAusK93SpU6T0KNV04qLnOC3Tx2vxesi9cdrTbldt047ZqUivIb7JJ2d3f1Yu8pOxsrWO6UP8McLyfh29iKxfHgr/LnitZacebUWjlY4aV/PzwKNnc2NZu0pzvLC7i5YpYm4ZXJr0rK59q9KE5Meav8AMnhvVNceTT3/AJccVLeH5/6xdF0WhQsK1xqFjOU6NRYjJyjN08QSwspYy2XzZ73zRTDbtU0+npjwTfNTrj/bqbHQ722ubW/pWVk6DjQkseK51ZThNJcub6vL2njmpfHkpbJbfr+zRgyY8mO9cVNur7oeG9Co07Wh8tsIyrVrmVF9NFKcYvc4v1YiW1Ge05J5O/VEb9Sum01K445WnXM7dbHsdGuaMtZo06FVW9W3uI2sebhOXNRUM9bw8Fr5qWjFaZjeJjdWmC9Zy1iJ4ZidmForvbajGjLQFcOLm+kqwW95lnD8V9WcF8vJZLcXK7KYeVx04eS3dDV1S8jZ0nDRk5VKlWNW1UfFhBPKk0l2mWMWOck75Oz5tk5ckYomMfb8nLatpl9fbKUNEp2T37pVYxjT5YaxJ8vF55xzfI2Y8mLDvacs28mLLiy5tojHFfN0OuXl5Cp8njosb+jQhShTrVI79/5uOWsry5X4GXFTHMcfKcMz8mvLfJE8MY+KI+bA4V0S7nqavqlhCwo04yXRxSinJ03DxY9eeeW+SPTUZscYeTi3FLz0+HJOblJrww9JOW6gAAAAAAAAAAAAADyXwiS/SNT7qh/Szu6DuY+svnviMf6ifpDm9xrYdld4GdZa3dUI7KNxOlBtycYYxufW+a9B53wY7zvaN3rjz5aRtW20J/yov/ttX2x7inNcP7YenOs/75/PRj3mvXdaDp1bmpUhLGYyaw8dWcItTBjpO9axurfPlvHDa0zC20167owVOldVKcFnEYtYWevGUL4Md53tWJkpny0jatpiFLviG8qwlTq3VSpCeN0ZbcPnnny8qFdPirO9a9abajLaNrWmYY9hq1xb7ugrTo78b9mPGxnGc+t+0tfFTJ+qN1aZb4/0TtumrcS30tu68qy2SU4Z2+LJJpNcvSysabFHZVedVmnttK78rNR+3VvbHuHNMP7YW53m/dK38rdR+3VvbHuHNMP7YOd5v3StfF2o/bq3tj3DmmH9sHO8/wC6UNfifUJrEr2s0mnhS2808rOOsmNNijsrCJ1Wae20r/yv1L7dW9se4jmmH9sJ53m/dLo/B1rV1caio17mrViqFZqM5PbnxeeFyyZNbhx0w71jbra9DmyZMv8AFO/U9XOO7IAAAAAAAAAAAAADyHwjv9I1PuqH9LO7oO5j6y+f+Id/P0hzO42MSu4BuApuApuAtcgLcgWuRItbAtbJFrYFrYStAtbA7DwU/SS/d63xiYfiPc+sN/w7vvR7OcJ3QAAAAAAAAAAAAAHjvhKf6SqfdUP6Wd3QdxH1l8/8Q7+fpDmMmxiMgMgMgUySbKZAtbJ2FMgWtgUbCVrYFrAo2BQDsPBT9JL93rfGJh+Idz6w3/Du+9Hs5wndAAAAAAAAAAAAAAeN+Ex/pKp91Q/pZ3vh/cR9ZcD4h33pDlsmzZhMjYMhJknZCmQGQLWwKZApkC1sJUYFGBaAA7DwUfSS/d63xiYfiPc+sN/w7vvR7QcJ3QAAAAAAAAAAAAAHnHG3CF7d3s69GNN05U6UU5VFF5iufLB1dJq8eLHw27XK1ekyZcnFXsaL5vNS+pR98u409IYfP7MvR2by+6nzeal5uj75dxHSGHz+yej83l91fm81LzdH3y7h0hh8/sdH5vL7nzeal9Sj75dxPSGHxn7I6OzeX3U+bzUvqUffLuHSGHz+x0dm8vufN3qX1KPvl3DpDD5/Y6OzeX3U+bvUvqUffLuI6Qw+f2Ojs3l91Pm71L6lH3y7iekMPn9k9H5vL7nzdan9Sj75dw6Qw+f2Oj83kp83Op+bo++XcOkMPn9jo7N5KfNzqfm6Pvl3DpDD4z9jo7N5Hzcan9Sj75dw6Qw+M/Y6OzeSnzcan5uj75dw6Qw+f2Ojs3kPwb6n9Sj75dw6Qw+f2Ojs3k6LgPg69s7xV68aap9FUh4lRTe54xyx6DLrNXjy4+Gvbu1aPSZMWTit4PSDluoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD//Z" alt="amex" />
                    <img height='100' width='100'  src="https://brand.mastercard.com/content/dam/mccom/brandcenter/other-marks/ms_acc_100_2x.png" alt="maestro" />
                </div>
            </div>

        <div class="row cardholder">
                <div class="info">
                    <label for="cardholdername">Name</label>
                    <input placeholder="e.g. Richard Bovell" id="cardholdername" type="text" required name="name" onChange={this.handleInputChange} />
                </div>
            </div>

        {/* <div class="row cardholder">
                <div class="info">
                    <label for="cardholdername">E-mail</label>
                    <input placeholder="e.g. Richard-Bovell@Example.com" id="cardholdername" type="email" required name="email" onChange={this.handleInputChange} />
                </div>
            </div> */}

        <div class="row cardholder">
                <div class="info">
                    <label for="cardholdername">Email</label>
                    <input placeholder="e.g. 619404313" id="cardholdername" required name="email" type="text" onChange={this.handleInputChange}  />
                </div>
            </div>
              
        <div class="row number">
                <div class="info">
                  <div className='cardnumbery'>
                    <label for="cardnumber">
                      Card number
                    </label>

                    <div className='cardnumberx'>
                      <CardNumberElement onChange={this.handleChange} />
                      </div></div>
      {/* <input id="cardnumber" type="text" pattern="[0-9]{16,19}" maxlength="19" placeholder="8888-8888-8888-8888"/> */}
                </div>
            </div>

        <div class="row details">
                <div class="left">
                  <div className='expiryx'>
                    <label for="expiry-date">Expiry</label>
                      <div>
                        <CardExpiryElement onChange={this.handleChange} />
                      </div> 
                </div></div>
                <div class="right">
                    <div className='cvcx'>
                    <label for="cvv">CVC/CVV</label>
                    <CardCVCElement onChange={this.handleChange} />
                    <span data-balloon-length="medium" data-balloon="The 3 or 4-digit number on the back of your card." data-balloon-pos="up">i</span>
                    </div>
                </div>
            </div>
   
    </div>
    
      <div class="button">
        <button type="submit"><i class="ion-locked"></i> Confirm and Pay {this.state.amount}$</button>
    </div>

    </div>


  </div>

</form>
    );
  }
}


export default injectStripe(CheckoutForm);
