import React , { useState } from 'react'
import Axios from 'axios' 
import {URL} from "../config.js";

import '../styles/admin.css';

const Login = (props) => {
	const [ form , setValues ] = useState({
		email    : '',
		password : ''
	})
	const [ message , setMessage ] = useState('')
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
          const response = await Axios.post(`${URL}/admin/login`,{
          	email:form.email,
          	password:form.password
          })
          setMessage(response.data.message)
          if( response.data.ok ){
              localStorage.setItem('token',JSON.stringify(response.data.token)) 
              setTimeout( ()=> props.history.push('/admin'),1)    
          }
          
		}
        catch(error){
        	console.log(error)
        }
	}
	return <form onSubmit={handleSubmit}
	             onChange={handleChange}
	             className='form_container'>
					 <div className='addbox'>
						 
						 <div className='addboxinner'>
						 <div className='addtitle'> Login to Admin Area </div>
						 <div className='loginx'>
	         <label className='loginbox'>Email</label>    
		     <input name="email" className='loginbox'/>
		     <label className='loginbox'>Password</label>
		     <input name="password" className='loginbox'/></div>
		     <button>login</button>
		     <div className='loginmsg'><h4>{message}</h4></div>
			 </div></div>
	       </form>
}

export default Login


