import React from "react";
import '../styles/reset.css';
import '../styles/home.css';

var BestSeller = (props) => {
    return (
        <div className="mainFlex2">
          {
          props.list.map((product, i) =>{
              if (product.bestSeller === true) {
            return(
              <div class='productBoxz'>
                <div><h1 class = "name">{product.product}</h1>
              <img src = {product.image} class = "img"></img>
              
              <h2  class = "price">${product.price}</h2></div>
              {product.onSale === true ?<img src = "https://cdn0.iconfinder.com/data/icons/shopping-icons-4/110/Tag-Sale-512.png" class = "onSaleTagx"></img> : null }
              </div>
            )}
            else { return null}
          })
        }
        </div>
      );}
export default BestSeller;