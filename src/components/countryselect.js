import React, { Component } from 'react';

// note that you can also export the source data via CountryRegionData. It's in a deliberately concise format to 
// keep file size down
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';
import '../styles/address.css';

class CountrySelect extends React.Component {
  constructor (props) {
    super(props);}


  render () {
    return (
      <div>
          <div className='selectx'>
        <CountryDropdown
          value={this.props.country}
          name={'country'}
          onChange ={(e)=>this.props.handleInputChangeCountry(e)}/></div>
          <div className='selectx' >
        <RegionDropdown
            name={'state'}
          country={this.props.country}
          value={this.props.region}
          onChange ={(e)=>this.props.handleInputChangeState(e)}/></div>
      </div>
    );
  }
}

export default CountrySelect