import React , { useEffect } from 'react'
import Axios from 'axios' 
import {URL} from "../config.js";

const SecretPage = (props) => {
	const token = JSON.parse(localStorage.getItem('token'))
	useEffect( () => {
		if( token === null )return props.history.push('/')
	},[token,props.history])
	const verify_token = async () => {
		try{
           const response = await Axios.post(`${URL}/admin/verify_token`,{token})
           return !response.data.ok
           ? props.history.push('/')
           : null
		}
		catch(error){
			console.log(error)
		}
	}
	verify_token()
	return <div className='secret_page'>
	          <h1>This is the secret page</h1>
	          <h2>You can access here only after verify the token</h2>
	          <button onClick={()=>{localStorage.removeItem('token');props.history.push('/')}}>logout</button>
	       </div>
}

export default SecretPage
