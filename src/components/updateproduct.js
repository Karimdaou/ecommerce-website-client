import React , { useState } from 'react'
import Axios from 'axios' 
import {URL} from "../config.js";

const UpdateProduct = () => {
	const [ form , setValues ] = useState({
		catID    : '',
		price : 0,
        color: '',
        product: '',
        _id: ''
	})
	const [ message , setMessage ] = useState('')
	const handleChange = e => {
	   setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		console.log(form)
		try{
			const response =  await Axios.post(`${URL}/products/update`,{
				_id    : form.x1,
				key1: form.x2,
				key2: form.x3,
				key3: form.x4,
			    value1: form.x21,
                value2: form.x31,
				value3: parseInt(form.x41),
				value4: form.x51,
				value5: form.x52
	        })
			setMessage(response.data.message)
			console.log(response.data.message,'holeeeeeeeeeeeeeeeeeeee')
		}
		catch( error ){
			console.log(error)
		}

	}
	return (
	<div className='addboxv'>
          
          <div className='addboxinnervu'>
          <div className='addtitle'>Update items in DB</div>
	<form onSubmit={handleSubmit}
	             onChange={handleChange}
	             className='form_container'>
			<div className='updt'>
	         <label>_id</label>
		     <input name="x1"/>
			 </div>
			 <div className='updt'>
             <label>Type: Text</label>
			 <input name="x2"/>
		     <input name="x21"/>
			 </div>
			 <div className='updt'>
		     <label>Type: Boolean</label>
			 <input name="x3"/>
		     <input name="x31"/>
			 </div>
			 <div className='updt'>
		     <label>Type: Number</label>
			 <input name="x4"/>
		     <input type="number" name="x41"/>
			 </div>
			 <div className='updt'>
			 <label>Carousel Img URL</label>
			 <input name="x51"/>
			 </div>
			 <div className='updt'>
			 <label>Carousel Thumbnail URL</label>
		     <input name="x52"/>
			 </div>

		     <div className='viewbutton'><button>update</button></div>









			 
		     <div className='adminmessagex'>

				{ message ? Object.entries(message).map(([key,value])=>{
				return <div className='viewmsg'>
					<div>{key}</div>
					<div>{value.toString()}</div>
				</div>
				}
				)
				: <div className='viewnotfoundx'>
					<div> Item does not exist</div>
						
					</div>
				}

				<div></div>
				 {/* <h4>{message}</h4> */}
				 </div>

		     {/* <div className='message'><h4>{[message]}</h4></div> */}
	       </form></div></div>
	)
}

export default UpdateProduct
