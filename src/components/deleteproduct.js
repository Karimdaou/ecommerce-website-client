import React , { useState } from 'react'
import Axios from 'axios' 
import {URL} from "../config.js";

const RemoveProduct = () => {
	const [ form , setValues ] = useState({
        product: ''
	})
	const [ message , setMessage ] = useState('')
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
			const response =  await Axios.post(`${URL}/products/remove`,{
                product: form.product
	        })
	        setMessage(response.data.message)
	        //console.log(response)
		}
		catch( error ){
			console.log(error)
		}

	}
	return <form onSubmit={handleSubmit}
	             onChange={handleChange}
	             className='form_container'>
					 <div className='addboxz'>
						 <div className='addxboxinner'>
						 <div className='addtitle'> Delete a Product from DB</div>
							 <div className='addinput'>
             <label>Product</label>
             <input name="product"/>
			 </div>
		     <button>delete</button>
			 <div className='adminmessage'><h4>{message}</h4></div>
			 	</div>
				 
			 </div>
		     
	       </form>
}

export default RemoveProduct
