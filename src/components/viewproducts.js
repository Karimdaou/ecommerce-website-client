import React , { useState } from 'react'
import Axios from 'axios' 
import {URL} from "../config.js";

const ViewProducts = () => {

    const [ message , setMessage ] = useState([])
    const [ messagex , setMessagex ] = useState('')
    const [ messagey , setMessagey ] = useState([])

    const handleChange = e => {
        setMessagex(e.target.value)
     }


const handleSubmit = async (e) => {
		e.preventDefault()
		try{
            const response =  await Axios.get(`${URL}/products/displayprods`)
            setMessage(response.data.message)
            console.log(message)
            
		}
		catch( error ){
			console.log(error)
		}

    }
    const handleSubmit2 = async (e) => {
		e.preventDefault()
		try{
            const response =  await Axios.get(`${URL}/products/getproducts/${messagex}`)
            setMessagey(response.data.message)
            console.log(message)
            
		}
		catch( error ){
			console.log(error)
		}

	}    
	return ( <div>
        <div className='addboxv'>
          
          <div className='addboxinnerv'>
          <div className='addtitle'>Display all items in DB</div>
        <form onSubmit={handleSubmit} >
            <div className='viewbutton'><button>GetAll</button></div>
        </form>
        <div className='viewmsgx'>
                {
                  message.map((product, i) => {
                      return  <div className='viewproductx'>
                          
                      <h1>Name:{product.product}</h1>
                      <h2>{product.catID}</h2>
                      <h2>Price:{product.price}$</h2>
                      </div>
                      
                  })}</div></div></div>



                  <div  className='addboxv'>
                    <div className='addboxinnerv'>
                    <div className='addtitle'>Display all items in a Category</div>
                <form onSubmit={handleSubmit2}
                      onChange={handleChange}>
                        <div className='viewcathead'>
            <div><label>Category</label><input/></div>

            <div><button>Get</button></div>
            </div>
        </form>
        <div className='viewspacer'>
        {messagey.map((product, i) => {
                      return  <div className='viewcatbox'>
                        <div className='catimage'><div className='imgxz'><img height='100%' width='100%' src={product.image}></img></div></div>
                      <div className='viewproductx'>  
                      <h1>{product.product}</h1>
                      <h1>{product.price}$</h1>
                      <h1>Stock:{product.stock}</h1>
                      <h1>ID:{product._id}</h1>
                      </div></div>
                  })}</div>
        </div>
        </div></div>

     ); }

export default ViewProducts
